class To7mMidiMessagesError(Exception):
    pass


class HashIndexError(To7mMidiMessagesError):
    pass
