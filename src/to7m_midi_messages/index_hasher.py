from math import ceil
from hashlib import sha256

from .exceptions import HashIndexError


class IndexHasher:
    def __getitem__(self, indices):
        self._check_indices(indices)
        *indices, slice_ = indices
        self._check_slice(slice_)

        hash_int = self._hash(indices)
        return self._fit_to_range(slice_, hash_int)

    def _check_indices(self, indices):
        if not isinstance(indices, tuple):
            raise HashIndexError(
                      f"at least 2 indices required: "
                      "1 or more inputs to hash, and finally a limits slice")

    def _check_slice(self, slice_):
        if not isinstance(slice_, slice):
            raise HashIndexError("last index must be a limits slice")

    def _hash(self, indices):
        main_hash_obj = sha256()
        for index in indices:
            index_hash_obj = sha256()
            index_bytes = repr(index).encode()
            index_hash_obj.update(index_bytes)
            main_hash_obj.update(index_hash_obj.digest())
        main_hash_bytes = main_hash_obj.digest()
        return int.from_bytes(main_hash_bytes, "big")

    def _fit_to_range(self, slice_, hash_int):
        start, stop, step = slice_.start, slice_.stop, slice_.step
        start = 0 if slice_.start is None else slice_.start
        if stop is None:
            raise HashIndexError("slice stop must be provided")
        step = 1 if slice_.step is None else slice_.step

        difference = stop - start
        if difference <= 0:
            raise HashIndexError("slice stop must be higher than slice start")

        base = ceil(difference / step)
        i = hash_int % base
        return start + step * i
